const {PubSub} = require ('@google-cloud/pubsub');
const cors = require ('cors')
const express = require ('express')

const app = express();

app.use(cors())
app.use(express.json())

const pubsub = new PubSub({projectId: 't2-issue-review-board'});

app.post('/submitissue', async (req, res) =>{

    if(!req.body.date_posted || !req.body.date_issue || !req.body.description || !req.body.location || !req.body.type){
        res.status(404).send("Missing information to submit issue")
    }

    const issue = {
        date_posted: req.body.date_posted,
        date_issue: req.body.date_issue,
        description: req.body.description,
        location: req.body.location,
        type: req.body.type
    }
    const response = await pubsub.topic('issue-topic').publishJSON(issue);

    res.status(201).send("Issue submitted successfully");
    console.log(response);
})

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{console.log(`Application started on port ${PORT}`)});
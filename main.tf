provider "google" {
    project = "t2-issue-review-board"
    region = "us-central1"
    zone = "us-central1-c"
}

resource "google_cloud_run_service" "T2-Issue-Review-Board" {
  name     = "cloudrun-srv"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "gcr.io/t2-issue-review-board/issue-ingestion-v9" 
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_pubsub_topic" "issue-topic-resource" {
  name = "issue-topic"
}

resource "google_pubsub_subscription" "issue-subscription-resource" {
  name  = "issue-subscription"
  topic = "${google_pubsub_topic.issue-topic-resource.name}"

  # 20 minutes
  message_retention_duration = "1200s"
  retain_acked_messages      = true

  ack_deadline_seconds = 20

  expiration_policy {
    ttl = "300000.5s"
  }
  retry_policy {
    minimum_backoff = "10s"
  }

  enable_message_ordering    = false
}

resource "google_cloud_run_service_iam_member" "run_all_users" {
  service  = google_cloud_run_service.T2-Issue-Review-Board.name
  location = google_cloud_run_service.T2-Issue-Review-Board.location
  role     = "roles/run.invoker"
  member   = "allUsers"
}
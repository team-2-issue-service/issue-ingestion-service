FROM node 
# FROM base image. (the foundational software that will be in the image and containers created from this image)
# Any image created from this dockerfile Will include node.js
COPY . /workspace
# COPY source destination
# source path to the local folder, Destination the file directory name in the container

WORKDIR /workspace
# The commands you run in you r dockerfile. What directory in the container do you want to run them from

EXPOSE 3000
# Port 3000 on a container created from this image is accessible
RUN npm install
# RUN is used for commands to execute in the container when being created

# A command to run when you create a container based off of this image
ENTRYPOINT [ "node", "index.js" ]